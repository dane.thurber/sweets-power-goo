import initStoryshots, {
  multiSnapshotWithOptions,
  Stories2SnapsConverter,
} from '@storybook/addon-storyshots'

const createNodeMock = () => document.createElement('div')

initStoryshots({
  test: multiSnapshotWithOptions({ createNodeMock }),
  stories2snapsConverter: new Stories2SnapsConverter({
    snapshotsDirName: '',
    snapshotExtension: '.tsx.snap',
  }),
})
