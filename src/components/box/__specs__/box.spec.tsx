import { render } from '@testing-library/react'

import { basic } from '../__stories__/box.story'

describe('Box', () => {
  it('EXAMPLE: should render', () => {
    const comp = render(basic())

    expect(comp).toBeDefined()
  })
})
