import React from 'react'

import { Box } from '..'

export default {
  component: Box,
  title: 'components|Box',
}

export const basic = () => {
  return <Box />
}
