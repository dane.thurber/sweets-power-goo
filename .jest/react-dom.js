import ReactDOM from 'react-dom'

beforeAll(() => {
  window.matchMedia = jest.fn().mockImplementation(query => {
    return {
      matches: false,
      media: query,
      onchange: null,
      addListener: jest.fn(), // deprecated
      removeListener: jest.fn(), // deprecated
      addEventListener: jest.fn(),
      removeEventListener: jest.fn(),
      dispatchEvent: jest.fn()
    }
  })

  ReactDOM.createPortal = jest.fn(el => el)
})

afterEach(() => {
  ReactDOM.createPortal.mockClear()
})
