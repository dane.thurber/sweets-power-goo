/* eslint-env node  */

// https://jestjs.io/docs/en/configuration.html#snapshotresolver-string
//
//
module.exports = {
  resolveSnapshotPath: (testPath, snapshotExtension) =>
    testPath.replace('__specs__', '__snapshots__') + snapshotExtension,

  // resolves from snapshot to test path
  resolveTestPath: (snapshotFilePath, snapshotExtension) =>
    snapshotFilePath
      .replace('__snapshots__', '__specs__')
      .slice(0, -snapshotExtension.length),

  testPathForConsistencyCheck:
    '<rootDir>/src/components/Box/__specs__/Box.spec.tsx'
}
