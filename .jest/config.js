/* eslint-env node  */

module.exports = {
  preset: 'ts-jest/presets/js-with-babel',
  roots: ['<rootDir>/src'],
  testEnvironment: 'jsdom',

  collectCoverage: false,
  collectCoverageFrom: [
    '<rootDir>/src/**/*.{js,jsx,ts,tsx}',
    '!<rootDir>/src/**/*.{spec,specs,story,stories}.*',
    '!**/node_modules/**'
  ],

  setupFilesAfterEnv: [
    '@testing-library/jest-dom/extend-expect',
    '<rootDir>./.jest/react-dom'
  ],

  snapshotResolver: '<rootDir>/.jest/snapshot.config.js',

  transform: {
    '^.+\\.(stories|story)\\.(ts|tsx)$':
      '@storybook/addon-storyshots/injectFileName',
    '^.+\\.mdx$': '@storybook/addon-docs/jest-transform-mdx'
  },

  watchPlugins: [
    'jest-watch-typeahead/filename',
    'jest-watch-typeahead/testname'
  ]
}
