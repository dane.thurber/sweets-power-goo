module.exports = {
  addons: ['@storybook/addon-docs'],

  stories: ['../src/**/*.(stories|story).(js|jsx|ts|tsx|mdx)'],

  webpackFinal: async config => {
    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      use: [
        {
          loader: require.resolve('ts-loader')
        },
        {
          loader: require.resolve('react-docgen-typescript-loader'),
          options: {
            propFilter: prop => {
              if (!prop?.parent) return true

              return (
                !prop.parent.fileName.includes('emotion') &&
                !prop.parent.fileName.includes('node_modules/@types/react/')
              )
            }
          }
        }
      ]
    })

    config.resolve.extensions.push('.ts', '.tsx')

    return config
  }
}
